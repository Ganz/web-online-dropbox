package pro.webonline.webonlinedropbox.network.models.requests

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.google.gson.annotations.SerializedName

@JsonPropertyOrder("LAST_NAME", "FIRST_NAME", "MIDDLE_NAME", "PHONE", "EMAIL", "GRAZHDANSTVO", "CITY_ID", "KUPON", "PASSWORD", "LOGIN")

data class RegistrationRequest(
    var LAST_NAME: String,
    var FIRST_NAME: String,
    var MIDDLE_NAME: String,
    var PHONE: String,
    var EMAIL: String,
    var GRAZHDANSTVO: Int,
    var CITY_ID: Int,
    var KUPON: String,
    var PASSWORD: String,
    var LOGIN: String
)