package pro.webonline.webonlinedropbox.network.repository

import pro.webonline.webonlinedropbox.network.services.UserApi

object ApiRepositoryProvider{
    fun provideApiRepository(): ApiRepository{
        return ApiRepository(UserApi.create())
    }
}