package pro.webonline.webonlinedropbox.network.services

import io.reactivex.Observable
import io.reactivex.Single
import org.json.JSONObject
import pro.webonline.webonlinedropbox.network.models.requests.RegistrationRequest
import pro.webonline.webonlinedropbox.network.models.responses.RegistrationResponse
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

//http://migrant.webonline.pro/rest/?method=register_user&json=%5B%7B%22LAST_NAME%22%3A%22%D0%9F%D1%8F%D1%82%D0%BD%D0%B8%D1%86%D0%BA%D0%B8%D0%B9%22%2C%22FIRST_NAME%22%3A%22%D0%92%D0%BB%D0%B0%D0%B4%D0%B8%D0%BC%D0%B8%D1%80%22%2C%22MIDDLE_NAME%22%3Anull%2C%22PHONE%22%3A%22%2B79687595965%22%2C%22EMAIL%22%3A%22vladimir198787%40mail.ru%22%2C%22GRAZHDANSTVO%22%3A2%2C%22CITY_ID%22%3A1%2C%22KUPON%22%3Anull%2C%22PASSWORD%22%3A%22d0970714757783e6cf17b26fb8e2298f%22%2C%22LOGIN%22%3A%22vvp198510%22%7D%5D&send=%D0%9E%D1%82%D0%BF%D1%80%D0%B0%D0%B2%D0%B8%D1%82%D1%8C





interface UserApi {

    @POST("?method=register_user")
    fun registerUser(@Query("json") registrationRequest: String,
                     @Query("send") send: String) : Single<RegistrationResponse>



    companion object Factory {
        fun create(): UserApi{
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://migrant.webonline.pro/rest/")
                .build()
            return retrofit.create(UserApi::class.java)
        }

    }
  /*  companion object {
        fun create(): UserApi{
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                    RxJava2CallAdapterFactory.create())
                .addConverterFactory(
                    GsonConverterFactory.create())
                .baseUrl("http://migrant.webonline.pro/rest/")
                .build()
            return retrofit.create(UserApi::class.java)
        }
    }*/
}