package pro.webonline.webonlinedropbox.database

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Person(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val name: String,
    val surname: String,
    val middleName: String,
    val phoneNumber: String,
    val email: String,
    val coupon: String,
    val login: String,
    val password: String)


data class PersonLogin(
    val login: String
)

data class PersonPassword(
    val password: String
)

