package pro.webonline.webonlinedropbox.extensions

import android.content.Context
import android.widget.EditText
import pro.webonline.webonlinedropbox.R
import java.util.regex.Pattern

fun EditText.setErrorWHenEmpty(context: Context) : Boolean{
    if (this.text.trim().isEmpty()){
        this.error = context.getString(R.string.empty_error)
        return true
    }
    this.error = null
    return false
}

fun EditText.setErrorWhenInvalidMail(context: Context) : Boolean{
    val pattern = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
            "\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(" +
            "\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+"
    val matcher = Pattern.compile(pattern).matcher(this.text.toString())
    return if (matcher.matches()) {
        this.error = null
        false
    } else {
        this.error = context.getString(R.string.incorrect_mail)
        true
    }
}

fun EditText.setErrorWhenNameIncorrect(context: Context) : Boolean{
    val pattern = "[а-яёА-ЯЁ]+"
    val matcher = Pattern.compile(pattern).matcher(this.text.toString())
    return if (matcher.matches()) {
        this.error = null
        false
    } else {
        this.error = context.getString(R.string.incorrect_name)
        true
    }
}