package pro.webonline.webonlinedropbox.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = [Person::class], version = 1)


abstract class PersonDatabase : RoomDatabase() {
    abstract fun personDao(): PersonDao

    companion object {
        var INSTANCE: PersonDatabase? = null

        fun getPersonDatabase(context: Context): PersonDatabase? {
            if (INSTANCE == null){
                synchronized(PersonDatabase::class){
                    INSTANCE =
                        Room.databaseBuilder(context.applicationContext, PersonDatabase::class.java, "myDB")
                            .allowMainThreadQueries().build()
                }
            }
            return INSTANCE
        }

        fun destroyDatabase(){
            INSTANCE = null
        }
    }
}