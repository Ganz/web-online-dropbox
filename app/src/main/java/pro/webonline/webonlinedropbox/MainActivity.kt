package pro.webonline.webonlinedropbox

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.delegateadapter.delegate.diff.DiffUtilCompositeAdapter
import kotlinx.android.synthetic.main.activity_main.*
import pro.webonline.webonlinedropbox.adapters.BottomNavigationAdapter
import pro.webonline.webonlinedropbox.adapters.FIleDelegateAdapter
import pro.webonline.webonlinedropbox.adapters.FolderDelegateAdapter
import pro.webonline.webonlinedropbox.extensions.SharedPreference
import pro.webonline.webonlinedropbox.extensions.inflate
import pro.webonline.webonlinedropbox.extensions.toast
import pro.webonline.webonlinedropbox.models.BottomNavigationModel
import pro.webonline.webonlinedropbox.models.FolderModel
import java.io.File
import java.io.FileOutputStream
import java.net.URI


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {


    private lateinit var listNavigation: MutableList<BottomNavigationModel>
    private lateinit var adapterFolders: DiffUtilCompositeAdapter
    private lateinit var adapterFiles: DiffUtilCompositeAdapter
    private lateinit var toggle: ActionBarDrawerToggle
    private lateinit var folderState: String
    private lateinit var folderPreviousState: String
    private lateinit var rootFolder: String

    var openFolder: Int = -1

    companion object {
        private val REQUEST_TAKE_PHOTO = 0
        private val REQUEST_SELECT_IMAGE_IN_ALBUM = 1
        private val REQUEST_SELECT_FILE_IN_STORAGE = 2
        //Permission code
        private val PERMISSION_CODE = 1001
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rootFolder = filesDir.toString()
        folderState = filesDir.toString()
        folderPreviousState = filesDir.toString()
        setSupportActionBar(toolbar)
        val sharedPreference: SharedPreference = SharedPreference(this)
        setupDrawers()
        createInitialFolders()
        bottomNavigationData()


        fabT.addOnStateChangeListener { open ->
            // do something
        }

        fabT.addOnMenuItemClickListener { fab, textView, itemId ->
            when (itemId) {
                R.id.one -> {
                    createFolderDialog()
                }
                R.id.two -> {
                    selectFileInStorage()
                }
                R.id.three -> {
                    pickImage()
                    //selectImageInAlbum()
                }
            }
        }

        /*if (sharedPreference.getValueString("username")==null){
            val intent = Intent(this, AuthorizationActivity::class.java)
            startActivity(intent)
            finish()
        }*/

        iv_profile.setOnClickListener {
            val intent = Intent(this, ProfileActivity::class.java)
            startActivity(intent)
        }

        /*val userLogin: String = intent.getStringExtra("person")
        Toast.makeText(this, userLogin, Toast.LENGTH_SHORT).show()*/

//Initializing of Folders' Adapter
        adapterFolders = DiffUtilCompositeAdapter.Builder()
            .add(FolderDelegateAdapter {clickFolder(it)})
            .build()
        rv_folders.layoutManager = GridLayoutManager(this, 3)
        rv_folders.adapter = adapterFolders
        adapterFolders.swapData(FolderDataFactory(folderState, rootFolder, folderPreviousState).generateFolderList())

//Initializing of Files' Adapter
        adapterFiles = DiffUtilCompositeAdapter.Builder()
            .add(FIleDelegateAdapter())
            .build()
        rv_files.layoutManager = GridLayoutManager(this, 4)
        rv_files.adapter = adapterFiles
        adapterFiles.swapData(FileDataFactory().generateFileList())

//Initializing of Bottom Navigation Adapter
        val myAdapter = BottomNavigationAdapter(listNavigation)
        rv_navigation.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rv_navigation.adapter = myAdapter
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_tech_support -> {
                val intent = Intent(baseContext, TechSupportActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_invite_friend -> {
                Toast.makeText(this, "Пригласить друга", Toast.LENGTH_SHORT).show()

                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.type = "text/plain"
                shareIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.")
                startActivity(Intent.createChooser(shareIntent, getString(R.string.send_to)))
            }
            R.id.nav_qa -> {
                val intent = Intent(baseContext, FAQActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_about_us -> {
                val intent = Intent(baseContext, AboutUsActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_commonwealth -> {
                val intent = Intent(baseContext, CommonwealthActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_basket -> {
                val intent = Intent(baseContext, BasketActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_settings -> {
                val intent = Intent(baseContext, SettingsActivity::class.java)
                startActivity(intent)
            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun bottomNavigationData() {
        listNavigation = ArrayList()
        listNavigation.add(
            BottomNavigationModel(
                R.drawable.ic_transfer_nav,
                "ПЕРЕВОД"
            )
        )
        listNavigation.add(
            BottomNavigationModel(
                R.drawable.ic_patent_nav,
                "ОПЛАТА ПАТЕНТА"
            )
        )
        listNavigation.add(
            BottomNavigationModel(
                R.drawable.ic_insurance_nav,
                "СТРАХОВКА"
            )
        )
        listNavigation.add(
            BottomNavigationModel(
                R.drawable.ic_work_nav,
                "РАБОТА"
            )
        )
    }

    private fun clickFolder(it: FolderModel) {
        folderPreviousState = folderState.substringBeforeLast("/")
        folderState = folderState+"/"+it.title
        adapterFolders.swapData(FolderDataFactory(folderState, rootFolder, folderPreviousState).generateFolderList())
        when { (it.title == "Назад") -> folderState = folderPreviousState }
        folderState.toast(this)
    }

    private fun backFolder() {
        adapterFolders.swapData(FolderDataFactory(folderState, rootFolder, folderPreviousState).generateFolderList())
    }

    private fun setupDrawers() {
        toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar,
            R.string.navigation_drawer_open, R.string.navigation_drawer_close)

        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)
        drawer_layout.addDrawerListener(toggle)
    }

    private fun folderReader(root: File) {
        val fileList: ArrayList<File> = ArrayList()
        val listAllFiles = root.listFiles()

        if(listAllFiles != null && listAllFiles.isNotEmpty()) {
            Log.w("downloadFileName", "OK")
            for (currentFile in listAllFiles) {
                if (currentFile.name.endsWith(".docx"))
                    //tv_file_list.text = currentFile.name
                Log.e("downloadFileName", currentFile.name)
                fileList.add(currentFile.absoluteFile)
                Log.w("downloadFileName", "" + currentFile.name)

            }
        }
        Log.w("fileList", "" + fileList.size)
    }

    private fun selectImageInAlbum() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        if (intent.resolveActivity(packageManager) != null) {
            startActivityForResult(intent, REQUEST_SELECT_IMAGE_IN_ALBUM)
        }
    }

    private fun selectFileInStorage() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        if (intent.resolveActivity(packageManager) != null) {
            startActivityForResult(intent, REQUEST_SELECT_FILE_IN_STORAGE)
        }
    }

    private fun createFolderDialog() {
        val alertDialog = AlertDialog.Builder(
            this, R.style.AlertDialogCustom)
        val settingsLayout = this.inflate(R.layout.dialog_create_folder)
        alertDialog.setView(settingsLayout)

        val dialog = alertDialog.create()
        dialog.window.decorView.setBackgroundResource(android.R.color.transparent)
        dialog.show()

        val folderName = settingsLayout.findViewById<EditText>(R.id.edt_create_folder).text
        settingsLayout.findViewById<Button>(R.id.btn_test).setOnClickListener {
            createFolder(folderName.toString())
            folderName.toast(this)
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).performClick()
        }
    }

    private fun createFolder(take: String) {
        val folder = File(folderState, take)
        folder.mkdirs()
        if (folder.exists()) {
            Log.e("testApp", "${folder.absolutePath} EXISTS")

            File(folderState).walk().forEach {
                println(it)
                Log.e("forEach", it.toString())
            }
        }
        else
        {
            Log.e("testApp", folder.toString() + "Does not EXIST")
        }
    }

    private fun createInitialFolders() {
        //folderState = filesDir.toString()

        File(folderState).walk().forEach {
            println(it)
            Log.e("forEach1", it.toString())
        }

        val folderPersonal = File(folderState, "Личные документы")
        val folderChecks = File(folderState, "Чеки")
        if (!folderPersonal.exists()) {
            folderPersonal.mkdirs()
            //Log.e("testApp", "${folderPersonal.absolutePath} EXISTS")

            File(folderState).walk().forEach {
                println(it)
                Log.e("forEach2", it.toString())}

        }
        if(!folderChecks.exists())
        {
            folderChecks.mkdirs()

            File(folderState).walk().forEach {
                println(it)
                Log.e("forEach3", it.toString())
            }
        }
        else {
            File(folderState).walk().forEach {
                println(it)
                Log.e("forEach4", it.toString())
            }
        }

        File(folderState).walk().forEach {
            println(it)
            Log.e("forEach5", it.toString())
        }
    }

    private fun saveTestImage(uri: URI) {
        //val bitmap = BitmapFactory.decodeFile("image/*", uri)

        try {
            var file = this.getDir("Images", Context.MODE_PRIVATE)
            file = File(file, "img.jpg")
            val out = FileOutputStream(file)
           // bitmap.compress(Bitmap.CompressFormat.JPEG, 85, out)
            out.flush()
            out.close()
            Log.i("Seiggailion", "Image saved.")
        } catch (e: Exception) {
            Log.i("Seiggailion", "Failed to save image.")
        }
    }

    private fun pickImage() {
        //check runtime permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED){
                //permission denied
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                //show popup to request runtime permission
                requestPermissions(permissions, PERMISSION_CODE)
            }
            else{
                //permission already granted
                pickImageFromGallery()
            }
        }
        else{
            //system OS is < Marshmallow
            pickImageFromGallery()
        }
    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, REQUEST_SELECT_IMAGE_IN_ALBUM)

        //val pathTest = intent.
        //pathTest.toast(this)
     //   intentni package qilib toastda joyini bilib korish
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.size >0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup granted
                    pickImageFromGallery()
                }
                else{
                    //permission from popup denied
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    //handle result of picked image
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_SELECT_IMAGE_IN_ALBUM){
            image_view.setImageURI(data?.data)
          val pathTest2 = data?.data?.path
            pathTest2?.toast(this)
        }
    }



  /*  @RequiresApi(Build.VERSION_CODES.M)
    private fun getFilePaths():ArrayList<String> {

        if ((checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) !== PackageManager.PERMISSION_GRANTED))
        {
            // Should we show an explanation?
            if (shouldShowRequestPermissionRationale(
                    Manifest.permission.READ_EXTERNAL_STORAGE))
            {
                // Explain to the user why we need to read the contacts
            }
            requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                PERMISSION_CODE)
            // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
            // app-defined int constant that should be quite unique

        }

        val u = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val projection = arrayOf(MediaStore.Images.ImageColumns.DATA)
        var c: Cursor? = null
        val dirList = TreeSet<String>()
        val resultIAV = ArrayList<String>()
        var directories:Array<String?> = emptyArray()
        if (u != null)
        {
            c = managedQuery(u, projection, null, null, null)
        }
        if ((c != null) && (c.moveToFirst()))
        {
            do
            {
                var tempDir = c.getString(0)
                tempDir = tempDir.substring(0, tempDir.lastIndexOf("/"))
                try
                {
                    dirList.add(tempDir)
                }
                catch (e:Exception) {
                }
            }
            while (c.moveToNext())
            directories = arrayOfNulls<String>(dirList.size)
            dirList.toArray(directories)
        }
        for (i in 0 until dirList.size)
        {
            val imageDir = File(directories[i])
            var imageList = imageDir.listFiles()
            if (imageList == null)
                continue
            for (imagePath in imageList)
            {
                try
                {
                    if (imagePath.isDirectory)
                    {
                        imageList = imagePath.listFiles()
                    }
                    if ((imagePath.name.contains(".jpg") || imagePath.name.contains(".JPG")
                                || imagePath.name.contains(".jpeg") || imagePath.name.contains(".JPEG")
                                || imagePath.name.contains(".png") || imagePath.name.contains(".PNG")
                                || imagePath.name.contains(".gif") || imagePath.name.contains(".GIF")
                                || imagePath.name.contains(".bmp") || imagePath.name.contains(".BMP")))
                    {
                        val path = imagePath.absolutePath
                        resultIAV.add(path)
                    }
                }
                catch (e:Exception) {
                    e.printStackTrace()
                }
                // }
            }
        }
        return resultIAV
    }*/

}
