package pro.webonline.webonlinedropbox.adapters

import com.example.delegateadapter.delegate.KDelegateAdapter
import kotlinx.android.synthetic.main.item_folder_cardview.*
import pro.webonline.webonlinedropbox.models.FolderModel
import pro.webonline.webonlinedropbox.R

class FolderDelegateAdapter(private val onFolderClick: (FolderModel) -> Unit)
    : KDelegateAdapter<FolderModel>(){

    override fun onBind(item: FolderModel, viewHolder: KViewHolder)= with(viewHolder) {
        tv_folder.text = item.title
        iv_folder.setOnClickListener { onFolderClick(item) }
        iv_folder.setImageResource(item.imageRes)
    }

    override fun isForViewType(items: List<*>, position: Int) = items[position] is FolderModel

    override fun getLayoutId(): Int = R.layout.item_folder_cardview
}