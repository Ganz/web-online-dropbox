package pro.webonline.webonlinedropbox.adapters

import com.example.delegateadapter.delegate.KDelegateAdapter
import kotlinx.android.synthetic.main.item_file_cardview.*
import pro.webonline.webonlinedropbox.models.FileModel
import pro.webonline.webonlinedropbox.R

class FIleDelegateAdapter(): KDelegateAdapter<FileModel>() {

    override fun onBind(item: FileModel, viewHolder: KViewHolder) = with(viewHolder) {
        tv_file.text = item.title
        iv_file.setImageResource(item.imageRes)
    }

    override fun isForViewType(items: List<*>, position: Int) = items[position] is FileModel

    override fun getLayoutId(): Int = R.layout.item_file_cardview

}