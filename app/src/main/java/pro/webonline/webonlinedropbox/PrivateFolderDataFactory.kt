package pro.webonline.webonlinedropbox

import com.example.delegateadapter.delegate.diff.IComparableItem
import pro.webonline.webonlinedropbox.models.FolderModel

class PrivateFolderDataFactory {

    fun generateFolderList(): List<IComparableItem> {
        val newFolderList: ArrayList<IComparableItem> = arrayListOf()

        newFolderList.add(
            FolderModel(
                0,
                "Паспорт",
                R.drawable.ic_check
            )
        )

        newFolderList.add(
            FolderModel(
                0,
                "ИНН",
                R.drawable.ic_personal_data
            )
        )

        newFolderList.add(
            FolderModel(
                0,
                "Водительское удостоверение",
                R.drawable.ic_check
            )
        )

        return newFolderList

    }
}