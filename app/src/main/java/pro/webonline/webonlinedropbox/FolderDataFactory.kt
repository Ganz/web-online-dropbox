package pro.webonline.webonlinedropbox

import android.util.Log
import com.example.delegateadapter.delegate.diff.IComparableItem
import pro.webonline.webonlinedropbox.extensions.toast
import pro.webonline.webonlinedropbox.models.FolderModel
import java.io.File

class FolderDataFactory(
    private var folderState: String,
    private val rootFolder: String,
    private val folderPreviousState: String) {


    fun generateFolderList(): List<IComparableItem> {
        val newFolderList: ArrayList<IComparableItem> = arrayListOf()

        val parentName = folderState
        val parentPreviousName = folderPreviousState

        if (folderState != rootFolder) {
            newFolderList.add(
                FolderModel(
                    0,
                    "Назад",
                    R.drawable.ic_check
                )
            )
        }
        Log.e("NOW", folderState)
        Log.e("BEFORE", folderPreviousState)

        if (folderState.substringAfterLast("/") == "Назад") {
            Log.e("CHECK!! PRESSING NAZAD", folderState.substringAfterLast("/"))
            //test
            File(folderPreviousState).walk().forEach {
                val name =  it.toString()
                val domain: String? = name.substringAfterLast("$parentPreviousName/")
                Log.e("PRESSING NAZAD", domain)
                if (domain != null) {
                    when {
                        (domain.isNotEmpty()) && !domain.contains("/") -> {
                            newFolderList.add(
                                FolderModel(
                                    0,
                                    domain.toString(),
                                    R.drawable.ic_check
                                )
                            )
                            Log.e("NEW FOLDER FACTORY", domain) }
                    }
                }
            }
        }
        else {
            File(folderState).walk().forEach {
                val name =  it.toString()
                val domain: String? = name.substringAfterLast("$parentName/")
                Log.e("EMPTY FOLDER", domain)
                if (domain != null) {
                    when {
                        (domain.isNotEmpty()) && !domain.contains("/") -> {
                            newFolderList.add(
                                FolderModel(
                                    0,
                                    domain.toString(),
                                    R.drawable.ic_check
                                )
                            )
                            Log.e("NEW FOLDER FACTORY", domain) }

                        /*(domain.isEmpty()) -> {
                            newFolderList.add(
                                FolderModel(
                                    0,
                                    "Пусто",
                                    R.drawable.ic_check
                                )
                            )
                            Log.e("NEW FOLDER FACTORY", domain) }*/
                    }
                }
            }
        }

        return newFolderList
    }
}