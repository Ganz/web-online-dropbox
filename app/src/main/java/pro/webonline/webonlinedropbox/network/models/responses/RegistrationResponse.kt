package pro.webonline.webonlinedropbox.network.models.responses

data class RegistrationResponse(
    var ID: Int,
    var token: String
)