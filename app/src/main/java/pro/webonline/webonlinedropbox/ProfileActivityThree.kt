package pro.webonline.webonlinedropbox

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_profile_three.*

class ProfileActivityThree : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_three)

        btn_profile_three_forward.setOnClickListener {
            val intent = Intent(this, ProfileActivityFour::class.java)
            startActivity(intent)
        }

        btn_profile_three_backward.setOnClickListener {
            val intent = Intent(this, ProfileActivityTwo::class.java)
            startActivity(intent)
        }
    }
}
