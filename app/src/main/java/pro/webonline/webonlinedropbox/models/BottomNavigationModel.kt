package pro.webonline.webonlinedropbox.models

data class BottomNavigationModel (
        var imageId:Int,
        var iconTitle: String
)