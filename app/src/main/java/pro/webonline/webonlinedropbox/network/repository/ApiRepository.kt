package pro.webonline.webonlinedropbox.network.repository

import io.reactivex.Single
import pro.webonline.webonlinedropbox.network.models.requests.RegistrationRequest
import pro.webonline.webonlinedropbox.network.models.responses.RegistrationResponse
import pro.webonline.webonlinedropbox.network.services.UserApi

class ApiRepository(private val userApi: UserApi){


    fun registerUser(registrationRequest: String, send: String):
            Single<RegistrationResponse> = userApi.registerUser(registrationRequest, "Отправить")


}