package pro.webonline.webonlinedropbox.database

import android.arch.persistence.room.*

@Dao
interface PersonDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPerson(person: Person)

    @Update
    fun updatePerson(person: Person)

    @Delete
    fun deletePerson(person: Person)

    @Query("SELECT login FROM Person WHERE login == :login")
    fun getPersonByLogin(login: String): List<PersonLogin>

    @Query("SELECT password FROM Person WHERE password == :password")
    fun getPersonByPassword(password: String): List<PersonPassword>

    @Query("SELECT * FROM Person WHERE login == :login AND password == :password")
    fun getPersonByBoth(login: String, password: String): List<Person>


    @Query("SELECT * FROM Person")
    fun getPersons(): List<Person>

}