package pro.webonline.webonlinedropbox.models

import android.support.annotation.DrawableRes
import com.example.delegateadapter.delegate.diff.IComparableItem

class FolderModel (
    val order: Int,
    val title: String,
    @DrawableRes val imageRes: Int): IComparableItem {

    override fun id(): Any = order
    override fun content(): Any = title + order + imageRes
}
