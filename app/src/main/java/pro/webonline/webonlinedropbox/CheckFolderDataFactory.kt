package pro.webonline.webonlinedropbox

import com.example.delegateadapter.delegate.diff.IComparableItem
import pro.webonline.webonlinedropbox.models.FolderModel

class CheckFolderDataFactory {

    fun generateFolderList(): List<IComparableItem> {
        val newFolderList: ArrayList<IComparableItem> = arrayListOf()

        newFolderList.add(
            FolderModel(
                0,
                "Патент",
                R.drawable.ic_check
            )
        )

        newFolderList.add(
            FolderModel(
                0,
                "Страховка",
                R.drawable.ic_personal_data
            )
        )

        newFolderList.add(
            FolderModel(
                0,
                "Денежные переводы",
                R.drawable.ic_check
            )
        )

        newFolderList.add(
            FolderModel(
                0,
                "ГИБДД",
                R.drawable.ic_check
            )
        )

        return newFolderList

    }
}