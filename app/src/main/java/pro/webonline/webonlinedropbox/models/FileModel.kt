package pro.webonline.webonlinedropbox.models

import android.support.annotation.DrawableRes
import com.example.delegateadapter.delegate.diff.IComparableItem

class FileModel (
    val title: String,
    @DrawableRes val imageRes: Int): IComparableItem {

    override fun id(): Any = title
    override fun content(): Any = title + imageRes
}