package pro.webonline.webonlinedropbox

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        btn_profileOne.setOnClickListener {
            val intent = Intent(this, ProfileActivityTwo::class.java)
            startActivity(intent)
        }


    }
}
