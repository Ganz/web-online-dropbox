package pro.webonline.webonlinedropbox

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_profile_two.*

class ProfileActivityTwo : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_two)

        btn_profile_two_forward.setOnClickListener {
            val intent = Intent(this, ProfileActivityThree::class.java)
            startActivity(intent)
        }

        btn_profile_two_backward.setOnClickListener {
            val intent = Intent(this, ProfileActivity::class.java)
            startActivity(intent)
        }


    }
}
