package pro.webonline.webonlinedropbox

import com.example.delegateadapter.delegate.diff.IComparableItem
import pro.webonline.webonlinedropbox.models.FileModel

class FileDataFactory {

    fun generateFileList(): List<IComparableItem> {
        val newFileList: ArrayList<IComparableItem> = arrayListOf()

        newFileList.add(
            FileModel(
                "file.xls",
                R.drawable.ic_xls
            )
        )


        newFileList.add(
            FileModel(
                "file.doc",
                R.drawable.ic_pdf
            )
        )


        newFileList.add(
            FileModel(
                "file.doc",
                R.drawable.ic_doc
            )
        )


        newFileList.add(
            FileModel(
                "file.doc",
                R.drawable.ic_xls
            )
        )


        newFileList.add(
            FileModel(
                "file.doc",
                R.drawable.ic_doc
            )
        )


        newFileList.add(
            FileModel(
                "file.doc",
                R.drawable.ic_doc
            )
        )


        newFileList.add(
            FileModel(
                "file.doc",
                R.drawable.ic_doc
            )
        )

        newFileList.add(
            FileModel(
                "file.xls",
                R.drawable.ic_xls
            )
        )


        newFileList.add(
            FileModel(
                "file.doc",
                R.drawable.ic_pdf
            )
        )


        newFileList.add(
            FileModel(
                "file.doc",
                R.drawable.ic_doc
            )
        )


        newFileList.add(
            FileModel(
                "file.doc",
                R.drawable.ic_xls
            )
        )


        newFileList.add(
            FileModel(
                "file.doc",
                R.drawable.ic_doc
            )
        )


        newFileList.add(
            FileModel(
                "file.doc",
                R.drawable.ic_doc
            )
        )


        newFileList.add(
            FileModel(
                "file.doc",
                R.drawable.ic_doc
            )
        )


        return newFileList

    }

}