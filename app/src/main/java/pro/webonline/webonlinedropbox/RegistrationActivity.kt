package pro.webonline.webonlinedropbox

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.system.Os.bind
import android.util.Log
import android.widget.Toast
import com.google.gson.GsonBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_registration.*
import pro.webonline.webonlinedropbox.database.Person
import pro.webonline.webonlinedropbox.database.PersonDao
import pro.webonline.webonlinedropbox.database.PersonDatabase
import pro.webonline.webonlinedropbox.extensions.setErrorWHenEmpty
import pro.webonline.webonlinedropbox.extensions.setErrorWhenInvalidMail
import pro.webonline.webonlinedropbox.extensions.setErrorWhenNameIncorrect
import pro.webonline.webonlinedropbox.network.models.requests.RegistrationRequest
import pro.webonline.webonlinedropbox.network.repository.ApiRepositoryProvider
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import pro.webonline.webonlinedropbox.network.services.UserApi
import java.util.*


class RegistrationActivity : AppCompatActivity() {

    private var db: PersonDatabase? = null
    private var personDao: PersonDao? = null
    /*private val userApi by lazy {
        UserApi.create()
    }
    private var disposable: Disposable? = null*/

    val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        val repository = ApiRepositoryProvider.provideApiRepository()

     /*   db = PersonDatabase.getPersonDatabase(context = this)
        personDao = db?.personDao()*/


        btn_register.setOnClickListener {
            if (!hasErrorsInForms()){
                val name = edt_name_register.text.toString()
                val surname = edt_surname_register.text.toString()
                val middlename = edt_middlename_register.text.toString()
                val phone = edt_number_register.text.toString()
                val email = edt_email_register.text.toString()
                val coupon = edt_coupon_register.text.toString()
                val login = edt_login_register.text.toString()
                val password = edt_password_register.text.toString()

                val registerRequest = RegistrationRequest(surname, name, middlename, phone, email,
                    2, 8, coupon, password, login)

                val mapper = jacksonObjectMapper()

               // mapper.writeValueAsString(registerRequest)
                val jsonRegister: String = "[{\"LAST_NAME\":\"$surname\",\"FIRST_NAME\":\"$name\", \"MIDDLE_NAME\":\"$middlename\", \"PHONE\":\"$phone\", \"EMAIL\":\"$email\", \"GRAZHDANSTVO\":2, \"CITY_ID\":1, \"KUPON\":\"$coupon\", \"PASSWORD\":\"$password\", \"LOGIN\":\"$login\"}]"
                println(jsonRegister)

                compositeDisposable.add(
                    repository.registerUser(jsonRegister, "Отправить")
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            result ->
                            Log.d("Result", "Success")
                        },{
                            error ->
                            error.printStackTrace()
                        })
                )

                /*registerUser(registerRequest)*/

                /*var person = Person(name = name, surname = surname, middleName = middlename,
                    phoneNumber = phone, email = email, coupon = coupon, login = login, password = password)

                with(personDao){
                    this?.insertPerson(person)

                }*/


            }
        }
    }

    private fun hasErrorsInForms(): Boolean{
        return (edt_surname_register.setErrorWHenEmpty(this)|| edt_surname_register.setErrorWhenNameIncorrect(this) ||
                edt_name_register.setErrorWHenEmpty(this)|| edt_name_register.setErrorWhenNameIncorrect(this) ||
                edt_middlename_register.setErrorWHenEmpty(this) || edt_middlename_register.setErrorWhenNameIncorrect(this)
                || edt_number_register.setErrorWHenEmpty(this) ||
                edt_email_register.setErrorWHenEmpty(this) || edt_email_register.setErrorWhenInvalidMail(this) ||
                edt_coupon_register.setErrorWHenEmpty(this) || edt_login_register.setErrorWHenEmpty(this)||
                edt_password_register.setErrorWHenEmpty(this))
    }

    /*private fun registerUser(registerRequest: RegistrationRequest){

                UserApi.registerUser("register_user", registerRequest, "Отправить")
                    .subscribe(

                    )}*/
    }


