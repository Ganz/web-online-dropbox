package pro.webonline.webonlinedropbox

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_authorization.*
import pro.webonline.webonlinedropbox.database.PersonDao
import pro.webonline.webonlinedropbox.database.PersonDatabase
import pro.webonline.webonlinedropbox.extensions.SharedPreference
import pro.webonline.webonlinedropbox.extensions.setErrorWHenEmpty

class AuthorizationActivity : AppCompatActivity() {


    private var db: PersonDatabase? = null
    private var personDao: PersonDao? = null




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authorization)
        setSupportActionBar(toolbar)

        val sharedPreference: SharedPreference = SharedPreference(this)



        db = PersonDatabase.getPersonDatabase(context = this)
        personDao = db?.personDao()

        tv_registration.setOnClickListener {
            val intent = Intent(baseContext, RegistrationActivity::class.java)
            startActivity(intent)
        }



        btn_enter.setOnClickListener {
            if (!hasErrorInForms()){
                val login = edt_login.text.toString()
                val password = edt_password.text.toString()

                val getAll = personDao!!.getPersonByBoth(login, password)

                if (getAll.isNotEmpty()){
                    sharedPreference.save("username", login)
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
                else{
                    Toast.makeText(this, "Неправильный логин или пароль!", Toast.LENGTH_SHORT).show()
                }

            }

        }

    }

    private fun hasErrorInForms(): Boolean{
        return (edt_login.setErrorWHenEmpty(this) || edt_password.setErrorWHenEmpty(this))
    }
}
