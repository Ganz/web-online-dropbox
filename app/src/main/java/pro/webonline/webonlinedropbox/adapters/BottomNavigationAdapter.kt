package pro.webonline.webonlinedropbox.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import pro.webonline.webonlinedropbox.models.BottomNavigationModel
import pro.webonline.webonlinedropbox.R

class BottomNavigationAdapter(
    private val data: List<BottomNavigationModel>
) : RecyclerView.Adapter<BottomNavigationAdapter.NavigationViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NavigationViewHolder {
        val inflater =LayoutInflater.from(parent.context)
        val view: View = inflater.inflate(R.layout.item_navigation_cardview, null)
        return NavigationViewHolder(view)
    }

    override fun onBindViewHolder(holder: NavigationViewHolder, position: Int) {
        holder.image.setImageResource(data[position].imageId)
        holder.title.text = data[position].iconTitle
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class NavigationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var image: ImageView = itemView.findViewById(R.id.iv_navigation)
        var title: TextView= itemView.findViewById(R.id.tv_navigation)
    }
}