package pro.webonline.webonlinedropbox.extensions


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import pro.webonline.webonlinedropbox.MainActivity

fun ViewGroup.inflate(layoutRes: Int): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, false)
}

fun Any.toast(context: Context, duration: Int = Toast.LENGTH_SHORT): Toast {
    return Toast.makeText(context, this.toString(), duration).apply { show() }
}

fun MainActivity.inflate(resourceId: Int): View {
    return this.layoutInflater.inflate(resourceId, null)
}

